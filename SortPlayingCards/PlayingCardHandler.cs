﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SortPlayingCards
{
    public class PlayingCardHandler
    {
        /// <summary>
        /// using sort method of list
        /// </summary>
        /// <param name="cards"></param>
        /// <returns></returns>
        public List<CardModel> DefautlSort(List<CardModel> cards)
        {
            #region print array to console
            string before = "";
            foreach (var item in cards)
            {
                before += "=>" + item.CardIndex + item.CardType;
            }
            Console.WriteLine("Before Sorted: " + before);
            #endregion print array to console

            List<CardModel> ret = new List<CardModel>();
            cards.Sort((x, y) => x.Score.CompareTo(y.Score));
            return cards;
        }

        /// <summary>
        /// using insertion sort algorithm
        /// </summary>
        /// <param name="cards"></param>
        public void InsertionSort(List<CardModel> cards)
        {
            #region print array to console
            string before = "";
            foreach (var item in cards)
            {
                before += "=>" + item.CardIndex + item.CardType;
            }
            Console.WriteLine("Before Sorted: " + before);
            #endregion print array to console

            var arraySize = cards.Count;
            
            CardModel lastElement;
            int j = 0;
            for (int i = 1; i < arraySize; i++)
            {
                lastElement = cards[i];
                j = i;
                while ((j > 0) && (cards[j - 1].Score > lastElement.Score))
                {
                    cards[j] = cards[j - 1];
                    j = j - 1;
                }
                cards[j] = lastElement;
            }

        }


        public List<CardModel> GenerateListCardRandom()
        {
            List<CardModel> listCardSorted = GenerateListCardSorted();            
            var j = 0;
            CardModel rd = null;
            Random random = new Random();

            for (int i = 1; i < listCardSorted.Count; i++)
            {
                int x = random.Next(i, listCardSorted.Count - 1);
                rd = listCardSorted[j];
                listCardSorted[j] = listCardSorted[x];
                listCardSorted[x] = rd;
            }
            return listCardSorted;
        }

        public List<CardModel> GenerateListCardSorted()
        {
            List<CardModel> listCardSorted = new List<CardModel>();
            foreach (var ki in CardModel.CardIndexList.Keys)
            {
                foreach (var kt in CardModel.CardTypeList.Keys)
                {
                    listCardSorted.Add(new CardModel()
                    {
                        CardIndex = ki,
                        CardType = kt
                    });
                }
            }
            return listCardSorted;
        }

    }
}
