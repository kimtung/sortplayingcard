﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SortPlayingCards
{
    public class CardModel
    {
        public static Dictionary<string, int> CardIndexList = new Dictionary<string, int>()
        {
            ["1"] = 1,
            ["2"] = 2,
            ["3"] = 3,
            ["4"] = 4,
            ["5"] = 5,
            ["6"] = 6,
            ["7"] = 7,
            ["8"] = 8,
            ["9"] = 9,
            ["10"] = 10,
            ["J"] = 11,
            ["Q"] = 12,
            ["K"] = 13
        };
        public static Dictionary<string, double> CardTypeList = new Dictionary<string, double>()
        {
            ["C"] = 0.1,
            ["D"] = 0.2,
            ["H"] = 0.3,
            ["S"] = 0.4
        };// clubs (♣), diamonds (♦), hearts (♥), and spades (♠)
        public double Score
        {
            get { return CardIndexList[CardIndex] + CardTypeList[CardType]; }   // get method
        }
        public string CardIndex { get; set; }
        public string CardType { get; set; }

        public bool Equal(CardModel otherModel)
        {
            if (this.CardIndex == otherModel.CardIndex &&
            this.CardType == otherModel.CardType)
            {
                return true;
            }
            return false;
        }
    }
}
