using Microsoft.VisualStudio.TestTools.UnitTesting;
using SortPlayingCards;
using System;
using System.Collections.Generic;

namespace SortPlayingCard.Test
{
    [TestClass]
    public class SortPlayingCardTest
    {
        private readonly PlayingCardHandler _playingCards;
        public SortPlayingCardTest()
        {
            _playingCards = new PlayingCardHandler();
        }

        /// <summary>
        /// create a random list and compare it with the sorted list
        /// using sort function of List object
        /// </summary>
        [TestMethod]
        public void TestMethod1()
        {            
            //sorted list
            List<CardModel> listCardSorted = _playingCards.GenerateListCardSorted();
            List<CardModel> listCardRandom = _playingCards.GenerateListCardRandom();
            bool isDiff = false;
            string message = "";
            
            _playingCards.DefautlSort(listCardRandom);

            for (int i = 0; i < listCardRandom.Count; i++)
            {
                if(listCardRandom[i].CardType != listCardSorted[i].CardType || listCardRandom[i].CardIndex != listCardSorted[i].CardIndex)
                {
                    isDiff = true;
                    message = $"Position: {i}; Expect: {listCardSorted[i].CardIndex}-{listCardSorted[i].CardType}; Actual:{listCardRandom[i].CardIndex}-{listCardRandom[i].CardType};";
                    break;
                }
            }
            
            Assert.IsFalse(isDiff, message);
        }

        /// <summary>
        /// create a random list and compare it with the sorted list
        /// using Insertion Sort Algorithm
        /// </summary>
        [TestMethod]
        public void TestMethod2()
        {
            List<CardModel> listCardSorted = _playingCards.GenerateListCardSorted();
            List<CardModel> listCardRandom = _playingCards.GenerateListCardRandom();
            bool isDiff = false;
            string message = "";

            _playingCards.InsertionSort(listCardRandom);

            for (int i = 0; i < listCardRandom.Count; i++)
            {
                if (listCardRandom[i].CardType != listCardSorted[i].CardType || listCardRandom[i].CardIndex != listCardSorted[i].CardIndex)
                {
                    isDiff = true;
                    message = $"Position: {i}; Expect: {listCardSorted[i].CardIndex}-{listCardSorted[i].CardType}; Actual:{listCardRandom[i].CardIndex}-{listCardRandom[i].CardType};";
                    break;
                }
            }

            Assert.IsFalse(isDiff, message);
        }


        /// <summary>
        /// Create 3 Random Lists, and commpare each element of those lists
        /// </summary>
        [TestMethod]
        public void TestMethod3()
        {
            List<CardModel> listCardRandom1 = _playingCards.GenerateListCardRandom();
            List<CardModel> listCardRandom2 = _playingCards.GenerateListCardRandom();
            List<CardModel> listCardRandom3 = _playingCards.GenerateListCardRandom();
            bool isDiff = false;
            string message = "";
            if(listCardRandom1.Count != listCardRandom2.Count || listCardRandom2.Count != listCardRandom3.Count || listCardRandom1.Count != 53)
            {

            }
            for (int i = 0; i < listCardRandom1.Count; i++)
            {
                if (!listCardRandom1[i].Equal(listCardRandom2[i]) || !listCardRandom2[i].Equal(listCardRandom3[i]))
                {
                    isDiff = true;
                    message = $"Position: {i}; listCardRandom1: {listCardRandom1[i].CardIndex}-{listCardRandom1[i].CardType}; listCardRandom2:{listCardRandom2[i].CardIndex}-{listCardRandom2[i].CardType}; listCardRandom3:{listCardRandom3[i].CardIndex}-{listCardRandom3[i].CardType};";
                    break;
                }
            }

            Assert.IsTrue(isDiff, message);
        }

    }
}
